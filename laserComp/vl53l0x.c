#include "vl53l0x.h"

uint8_t g_i2cAddr = ADDRESS_DEFAULT;
uint16_t g_ioTimeout = 0;  // no timeout
uint8_t g_isTimeout = 0;
uint16_t g_timeoutStartMs;
uint8_t g_stopVariable; // read by init and used when starting measurement; is StopVariable field of VL53L0X_DevData_t structure in API
uint32_t g_measTimBudUs;
uint32_t measurement_timing_budget_us;

// LEGATO I2C

// Funcao de acesso ao dispositivo
// Parâmetros: i2c Bus e endereço do Dispositivo
// Retorna o arquivo de acesso
static int I2cAccessBusAddr(uint8_t i2cBus,uint8_t i2cAddr)
{
    const size_t filenameSize = 32;
    char filename[filenameSize];

    snprintf(filename, filenameSize, "/dev/i2c/%d", i2cBus);

    LE_DEBUG("Opening I2C bus: '%s'", filename);
    int fd = open(filename, O_RDWR);
    if (fd < 0 && (errno == ENOENT || errno == ENOTDIR))
    {
        snprintf(filename, filenameSize, "/dev/i2c-%d", i2cBus);
        LE_DEBUG("Opening I2C bus: '%s'", filename);
        fd = open(filename, O_RDWR);
    }

    if (fd < 0)
    {
        if (errno == ENOENT)
        {
            LE_ERROR(
                "Could not open file /dev/i2c-%d or /dev/i2c/%d: %s",
                i2cBus,
                i2cBus,
                strerror(ENOENT));
        }
        else
        {
            LE_ERROR("Could not open file %s': %s", filename, strerror(errno));
        }

        return LE_FAULT;
    }

    if (ioctl(fd, I2C_SLAVE, i2cAddr) < 0)
    {
        LE_ERROR("Could not set address to 0x%02x: %s", i2cAddr, strerror(errno));
        close(fd);
        return LE_FAULT;
    }

    return fd;
}

// Funcao de escrita BYTE
// Parâmetros: i2c Bus, endereço do Dispositivo, Registrador de destino e dado para escrita
// Retorna le_result_t
static le_result_t SmbusWriteReg(uint8_t i2cBus, uint8_t i2cAddr, uint8_t reg, uint8_t data)
{
    int i2cFd = I2cAccessBusAddr(i2cBus, i2cAddr);

    if (i2cFd == LE_FAULT)
    {
        LE_ERROR("failed to open i2c bus %d for access to address %d", i2cBus, i2cAddr);
        return LE_FAULT ;
    }

    le_result_t result;

    const int writeResult = i2c_smbus_write_byte_data(i2cFd, reg, data);

    if (writeResult < 0)
    {
        LE_ERROR("smbus write failed with error %d", writeResult);
        result = LE_FAULT;
    }
    else
    {
        LE_DEBUG("SMBUS Write addr 0x%x, reg=0x%x, data=0x%x", i2cAddr, reg, data);
        result = LE_OK;
    }

    close(i2cFd);

    return result;
}

// Funcao de escrita Word 16bits
// Parâmetros: i2c Bus, endereço do Dispositivo, Registrador de destino e dado para escrita
// Retorna le_result_t
static le_result_t SmbusWriteReg16Bits(uint8_t i2cBus, uint8_t i2cAddr, uint8_t reg, uint16_t data)
{
    int i2cFd = I2cAccessBusAddr(i2cBus, i2cAddr);

    if (i2cFd == LE_FAULT)
    {
        LE_ERROR("failed to open i2c bus %d for access to address %d", i2cBus, i2cAddr);
        return LE_FAULT ;
    }

    le_result_t result;

    const int writeResult = i2c_smbus_write_word_data(i2cFd, reg, data);

    if (writeResult < 0)
    {
        LE_ERROR("smbus write failed with error %d", writeResult);
        result = LE_FAULT;
    }
    else
    {
        LE_DEBUG("SMBUS Write addr 0x%x, reg=0x%x, data=0x%x", i2cAddr, reg, data);
        result = LE_OK;
    }

    close(i2cFd);

    return result;
}

// Funcao de escrita Block 32bits
// Parâmetros: i2c Bus, endereço do Dispositivo, Registrador de destino e dado para escrita
// Retorna le_result_t
static le_result_t SmbusWriteReg32Bits(uint8_t i2cBus, uint8_t i2cAddr, uint8_t reg, uint8_t len, uint8_t *data)
{
    int i2cFd = I2cAccessBusAddr(i2cBus, i2cAddr);

    if (i2cFd == LE_FAULT)
    {
        LE_ERROR("failed to open i2c bus %d for access to address %d", i2cBus, i2cAddr);
        return LE_FAULT ;
    }

    le_result_t result;

    const int writeResult = i2c_smbus_write_block_data(i2cFd, reg, len, data);

    if (writeResult < 0)
    {
        LE_ERROR("smbus write failed with error %d", writeResult);
        result = LE_FAULT;
    }
    else
    {
        LE_DEBUG("SMBUS Write addr 0x%x, reg=0x%x, data=0x%x", i2cAddr, reg, *data);
        result = LE_OK;
    }

    close(i2cFd);

    return result;
}

// Funcao de leitura
// Parâmetros: i2c Bus, endereço do Dispositivo, Registrador de origem, ponteiro para salvar o dado lido
// Retorna le_result_t
static le_result_t SmbusReadReg(uint8_t i2cBus, uint8_t i2cAddr, uint8_t  reg, uint8_t *data)
{
    int i2cFd = I2cAccessBusAddr(i2cBus, i2cAddr);
    if (i2cFd < 0)
    {
        LE_ERROR("failed to open i2c bus %d for access to address %d", i2cBus, i2cAddr);
        return  LE_FAULT;
    }

    le_result_t result;

    const int readResult = i2c_smbus_read_byte_data(i2cFd, reg);

    if (readResult < 0)
    {
        LE_ERROR("smbus read failed with error %d", readResult);
        result = LE_FAULT;
    }
    else
    {
        *data = readResult;
        LE_DEBUG("SMBUS READ addr=0x%x, reg=0x%x, data=0x%x", i2cAddr, reg, *data);
        result = LE_OK;
    }

    close(i2cFd);

    return result;
}

// Funcao de leitura 16bots
// Parâmetros: i2c Bus, endereço do Dispositivo, Registrador de origem, ponteiro para salvar o dado lido
// Retorna le_result_t
static le_result_t SmbusReadReg16Bits(uint8_t i2cBus, uint8_t i2cAddr, uint8_t  reg, uint16_t *data)
{
    int i2cFd = I2cAccessBusAddr(i2cBus, i2cAddr);
    if (i2cFd < 0)
    {
        LE_ERROR("failed to open i2c bus %d for access to address %d", i2cBus, i2cAddr);
        return  LE_FAULT;
    }

    le_result_t result;

    const int readResult = i2c_smbus_read_word_data(i2cFd, reg);

    if (readResult < 0)
    {
        LE_ERROR("smbus read failed with error %d", readResult);
        result = LE_FAULT;
    }
    else
    {
        *data = readResult;
        LE_DEBUG("SMBUS READ addr=0x%x, reg=0x%x, data=0x%x", i2cAddr, reg, *data);
        result = LE_OK;
    }

    close(i2cFd);

    return result;
}



long long millis() {
    le_clk_Time_t t = le_clk_GetAbsoluteTime();
    time_t s = t.sec;
    long us = t.usec;
    return (s*1000 + us/1000);
}

void getSequenceStepEnables(SequenceStepEnables * enables, uint8_t i2cBus, uint8_t i2cAddr)
{
    uint8_t sequence_config;
    SmbusReadReg(i2cBus, i2cAddr, SYSTEM_SEQUENCE_CONFIG, &sequence_config);
    enables->tcc          = (sequence_config >> 4) & 0x1;
    enables->dss          = (sequence_config >> 3) & 0x1;
    enables->msrc         = (sequence_config >> 2) & 0x1;
    enables->pre_range    = (sequence_config >> 6) & 0x1;
    enables->final_range  = (sequence_config >> 7) & 0x1;
}

uint16_t decodeTimeout(uint16_t reg_val)
{
  // format: "(LSByte * 2^MSByte) + 1"
  return (uint16_t)((reg_val & 0x00FF) <<
         (uint16_t)((reg_val & 0xFF00) >> 8)) + 1;
}

uint32_t timeoutMclksToMicroseconds(uint16_t timeout_period_mclks, uint8_t vcsel_period_pclks)
{
  uint32_t macro_period_ns = calcMacroPeriod(vcsel_period_pclks);

  return ((timeout_period_mclks * macro_period_ns) + (macro_period_ns / 2)) / 1000;
}

uint8_t getVcselPulsePeriod(vcselPeriodType type, uint8_t i2cBus, uint8_t i2cAddr)
{
    uint8_t tmp;
  if (type == VcselPeriodPreRange)
  {
        SmbusReadReg(i2cBus, i2cAddr, PRE_RANGE_CONFIG_VCSEL_PERIOD, &tmp);
        return decodeVcselPeriod(tmp);
  }
  else if (type == VcselPeriodFinalRange)
  {
      SmbusReadReg(i2cBus, i2cAddr, FINAL_RANGE_CONFIG_VCSEL_PERIOD, &tmp);
      return decodeVcselPeriod(tmp);
  }
  else { return 255; }
}

void getSequenceStepTimeouts(SequenceStepEnables const * enables, SequenceStepTimeouts * timeouts, uint8_t i2cBus, uint8_t i2cAddr)
{
    timeouts->pre_range_vcsel_period_pclks = getVcselPulsePeriod(VcselPeriodPreRange, i2cBus, i2cAddr);

    uint8_t tmp;
    SmbusReadReg(i2cBus, i2cAddr, MSRC_CONFIG_TIMEOUT_MACROP, &tmp);
    timeouts->msrc_dss_tcc_mclks = tmp + 1;
    timeouts->msrc_dss_tcc_us =
    timeoutMclksToMicroseconds(timeouts->msrc_dss_tcc_mclks,
                               timeouts->pre_range_vcsel_period_pclks);

    uint16_t tmp16;
    SmbusReadReg16Bits(i2cBus, i2cAddr, PRE_RANGE_CONFIG_TIMEOUT_MACROP_HI, &tmp16);
    timeouts->pre_range_mclks = decodeTimeout(tmp16);
    timeouts->pre_range_us =
        timeoutMclksToMicroseconds(timeouts->pre_range_mclks,
                               timeouts->pre_range_vcsel_period_pclks);

    timeouts->final_range_vcsel_period_pclks = getVcselPulsePeriod(VcselPeriodFinalRange, i2cBus, i2cAddr);

    SmbusReadReg16Bits(i2cBus, i2cAddr, FINAL_RANGE_CONFIG_TIMEOUT_MACROP_HI, &tmp16);
    timeouts->final_range_mclks = decodeTimeout(tmp16);

    if (enables->pre_range)
    {
        timeouts->final_range_mclks -= timeouts->pre_range_mclks;
    }

    timeouts->final_range_us =
        timeoutMclksToMicroseconds(timeouts->final_range_mclks,
                               timeouts->final_range_vcsel_period_pclks);
}

bool setSignalRateLimit(float limit_Mcps, uint8_t i2cBus, uint8_t i2cAddr)
{
    if (limit_Mcps < 0 || limit_Mcps > 511.99) { return false; }

    // Q9.7 fixed point format (9 integer bits, 7 fractional bits)
    SmbusWriteReg16Bits(i2cBus, i2cAddr, FINAL_RANGE_CONFIG_MIN_COUNT_RATE_RTN_LIMIT, limit_Mcps * (1 << 7));
    return true;
}

bool getSpadInfo(uint8_t *count, bool *type_is_aperture, uint8_t i2cBus, uint8_t i2cAddr)
{
    uint8_t tmp;

    SmbusWriteReg(i2cBus, i2cAddr,0x80, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr,0x00, 0x00);

    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x06);
    SmbusReadReg(i2cBus, i2cAddr, 0x83, &tmp);
    SmbusWriteReg(i2cBus, i2cAddr,0x83, tmp | 0x04);
    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x07);
    SmbusWriteReg(i2cBus, i2cAddr,0x81, 0x01);

    SmbusWriteReg(i2cBus, i2cAddr,0x80, 0x01);

    SmbusWriteReg(i2cBus, i2cAddr,0x94, 0x6b);
    SmbusWriteReg(i2cBus, i2cAddr,0x83, 0x00);

    startTimeout();
    SmbusReadReg(i2cBus, i2cAddr, 0x83, &tmp);
    while (tmp == 0x00)
    {
        if (checkTimeoutExpired()) { return false; }
        SmbusReadReg(i2cBus, i2cAddr, 0x83, &tmp);
    }
    SmbusWriteReg(i2cBus, i2cAddr,0x83, 0x01);
    SmbusReadReg(i2cBus, i2cAddr, 0x92, &tmp);

    *count = tmp & 0x7F;
    *type_is_aperture = (tmp >> 7) & 0x01;

    SmbusWriteReg(i2cBus, i2cAddr,0x81, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x06);
    SmbusReadReg(i2cBus, i2cAddr, 0x83, &tmp);
    SmbusWriteReg(i2cBus, i2cAddr,0x83, tmp  & ~0x04);
    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr,0x00, 0x01);

    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x80, 0x00);

    return true;
}

uint32_t getMeasurementTimingBudget(uint8_t i2cBus, uint8_t i2cAddr)
{
    SequenceStepEnables enables;
    SequenceStepTimeouts timeouts;

    uint16_t const StartOverhead     = 1910;
    uint16_t const EndOverhead        = 960;
    uint16_t const MsrcOverhead       = 660;
    uint16_t const TccOverhead        = 590;
    uint16_t const DssOverhead        = 690;
    uint16_t const PreRangeOverhead   = 660;
    uint16_t const FinalRangeOverhead = 550;

    // "Start and end overhead times always present"
    uint32_t budget_us = StartOverhead + EndOverhead;

    getSequenceStepEnables(&enables, i2cBus, i2cAddr);
    getSequenceStepTimeouts(&enables, &timeouts, i2cBus, i2cAddr);

    if (enables.tcc)
    {
        budget_us += (timeouts.msrc_dss_tcc_us + TccOverhead);
    }

    if (enables.dss)
    {
        budget_us += 2 * (timeouts.msrc_dss_tcc_us + DssOverhead);
    }
    else if (enables.msrc)
    {
        budget_us += (timeouts.msrc_dss_tcc_us + MsrcOverhead);
    }

    if (enables.pre_range)
    {
        budget_us += (timeouts.pre_range_us + PreRangeOverhead);
    }

    if (enables.final_range)
    {
        budget_us += (timeouts.final_range_us + FinalRangeOverhead);
    }

    measurement_timing_budget_us = budget_us; // store for internal reuse
    return budget_us;
}

uint16_t encodeTimeout(uint16_t timeout_mclks)
{
  // format: "(LSByte * 2^MSByte) + 1"

    uint32_t ls_byte = 0;
    uint16_t ms_byte = 0;

    if (timeout_mclks > 0)
    {
        ls_byte = timeout_mclks - 1;

        while ((ls_byte & 0xFFFFFF00) > 0)
        {
            ls_byte >>= 1;
            ms_byte++;
        }

        return (ms_byte << 8) | (ls_byte & 0xFF);
    }
    else { return 0; }
}

uint32_t timeoutMicrosecondsToMclks(uint32_t timeout_period_us, uint8_t vcsel_period_pclks)
{
  uint32_t macro_period_ns = calcMacroPeriod(vcsel_period_pclks);

  return (((timeout_period_us * 1000) + (macro_period_ns / 2)) / macro_period_ns);
}

bool setMeasurementTimingBudget(uint32_t budget_us, uint8_t i2cBus, uint8_t i2cAddr)
{
    SequenceStepEnables enables;
    SequenceStepTimeouts timeouts;

    uint16_t const StartOverhead      = 1910; // note that this is different than the value in get_
    uint16_t const EndOverhead        = 960;
    uint16_t const MsrcOverhead       = 660;
    uint16_t const TccOverhead        = 590;
    uint16_t const DssOverhead        = 690;
    uint16_t const PreRangeOverhead   = 660;
    uint16_t const FinalRangeOverhead = 550;

    uint32_t const MinTimingBudget = 20000;

    if (budget_us < MinTimingBudget) { return false; }

    uint32_t used_budget_us = StartOverhead + EndOverhead;

    getSequenceStepEnables(&enables, i2cBus, i2cAddr);
    getSequenceStepTimeouts(&enables, &timeouts, i2cBus, i2cAddr);

    if (enables.tcc)
    {
        used_budget_us += (timeouts.msrc_dss_tcc_us + TccOverhead);
    }

    if (enables.dss)
    {
        used_budget_us += 2 * (timeouts.msrc_dss_tcc_us + DssOverhead);
    }
    else if (enables.msrc)
    {
        used_budget_us += (timeouts.msrc_dss_tcc_us + MsrcOverhead);
    }

    if (enables.pre_range)
    {
        used_budget_us += (timeouts.pre_range_us + PreRangeOverhead);
    }

    if (enables.final_range)
    {
        used_budget_us += FinalRangeOverhead;

        // "Note that the final range timeout is determined by the timing
        // budget and the sum of all other timeouts within the sequence.
        // If there is no room for the final range timeout, then an error
        // will be set. Otherwise the remaining time will be applied to
        // the final range."

        if (used_budget_us > budget_us)
        {
        // "Requested timeout too big."
        return false;
        }

        uint32_t final_range_timeout_us = budget_us - used_budget_us;

        // set_sequence_step_timeout() begin
        // (SequenceStepId == VL53L0X_SEQUENCESTEP_FINAL_RANGE)

        // "For the final range timeout, the pre-range timeout
        //  must be added. To do this both final and pre-range
        //  timeouts must be expressed in macro periods MClks
        //  because they have different vcsel periods."

        uint16_t final_range_timeout_mclks =
        timeoutMicrosecondsToMclks(final_range_timeout_us,
                                    timeouts.final_range_vcsel_period_pclks);

        if (enables.pre_range)
        {
        final_range_timeout_mclks += timeouts.pre_range_mclks;
        }

        SmbusWriteReg16Bits(i2cBus, i2cAddr, FINAL_RANGE_CONFIG_TIMEOUT_MACROP_HI, encodeTimeout(final_range_timeout_mclks));

        // set_sequence_step_timeout() end

        g_measTimBudUs = budget_us; // store for internal reuse
    }
    return true;
}

bool performSingleRefCalibration(uint8_t vhv_init_byte, uint8_t i2cBus, uint8_t i2cAddr)
{
    SmbusWriteReg(i2cBus, i2cAddr, SYSRANGE_START, 0x01 | vhv_init_byte);

    startTimeout();
    uint8_t tmp;
    SmbusReadReg(i2cBus, i2cAddr, RESULT_INTERRUPT_STATUS, &tmp);
    while ((tmp & 0x07) == 0)
    {
        if (checkTimeoutExpired()) { return false; }
        SmbusReadReg(i2cBus, i2cAddr, RESULT_INTERRUPT_STATUS, &tmp);
    }

    SmbusWriteReg(i2cBus, i2cAddr,SYSTEM_INTERRUPT_CLEAR, 0x01);

    SmbusWriteReg(i2cBus, i2cAddr,SYSRANGE_START, 0x00);

    return true;
}












// VL53L0X Functions

// Funcao de inicialização do Sensor
bool initVL53L0X(uint8_t i2cBus,uint8_t i2cAddr)
{
    SmbusWriteReg(i2cBus, i2cAddr, 0x88, 0x00);

    SmbusWriteReg(i2cBus, i2cAddr, 0x80, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr, 0xFF, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr, 0x00, 0x00);
    SmbusReadReg(i2cBus, i2cAddr, 0x91, &g_stopVariable);
    SmbusWriteReg(i2cBus, i2cAddr, 0x00, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr, 0xFF, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr, 0x80, 0x00);

    // disable SIGNAL_RATE_MSRC (bit 1) and SIGNAL_RATE_PRE_RANGE (bit 4) limit checks
    uint8_t tmp;
    SmbusReadReg(i2cBus, i2cAddr, MSRC_CONFIG_CONTROL, &tmp);
    SmbusWriteReg(i2cBus, i2cAddr,MSRC_CONFIG_CONTROL, tmp | 0x12);

    // set final range signal rate limit to 0.25 MCPS (million counts per second)
    setSignalRateLimit(0.25, i2cBus, i2cAddr);


    SmbusWriteReg(i2cBus, i2cAddr, SYSTEM_SEQUENCE_CONFIG, 0xFF);

    // VL53L0X_DataInit() end

    // VL53L0X_StaticInit() begin

    uint8_t spad_count;
    bool spad_type_is_aperture;
    if (!getSpadInfo(&spad_count, &spad_type_is_aperture, i2cBus, i2cAddr)) 
    { 
        return false; 
    }

    // The SPAD map (RefGoodSpadMap) is read by VL53L0X_get_info_from_device() in
    // the API, but the same data seems to be more easily readable from
    // GLOBAL_CONFIG_SPAD_ENABLES_REF_0 through _6, so read it from there
    uint8_t ref_spad_map[6];
    SmbusReadReg(i2cBus, i2cAddr, GLOBAL_CONFIG_SPAD_ENABLES_REF_0, &ref_spad_map[0]);
    SmbusReadReg(i2cBus, i2cAddr, GLOBAL_CONFIG_SPAD_ENABLES_REF_0, &ref_spad_map[1]);
    SmbusReadReg(i2cBus, i2cAddr, GLOBAL_CONFIG_SPAD_ENABLES_REF_0, &ref_spad_map[2]);
    SmbusReadReg(i2cBus, i2cAddr, GLOBAL_CONFIG_SPAD_ENABLES_REF_0, &ref_spad_map[3]);
    SmbusReadReg(i2cBus, i2cAddr, GLOBAL_CONFIG_SPAD_ENABLES_REF_0, &ref_spad_map[4]);
    SmbusReadReg(i2cBus, i2cAddr, GLOBAL_CONFIG_SPAD_ENABLES_REF_0, &ref_spad_map[5]);

  // -- VL53L0X_set_reference_spads() begin (assume NVM values are valid)
    SmbusWriteReg(i2cBus, i2cAddr, 0xFF, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr, DYNAMIC_SPAD_REF_EN_START_OFFSET, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr, DYNAMIC_SPAD_NUM_REQUESTED_REF_SPAD, 0x2C);
    SmbusWriteReg(i2cBus, i2cAddr, 0xFF, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr, GLOBAL_CONFIG_REF_EN_START_SELECT, 0xB4);

    uint8_t first_spad_to_enable = spad_type_is_aperture ? 12 : 0; // 12 is the first aperture spad
    uint8_t spads_enabled = 0;

    for (uint8_t i = 0; i < 48; i++)
    {
        if (i < first_spad_to_enable || spads_enabled == spad_count)
        {
            // This bit is lower than the first one that should be enabled, or
            // (reference_spad_count) bits have already been enabled, so zero this bit
            ref_spad_map[i / 8] &= ~(1 << (i % 8));
        }
        else if ((ref_spad_map[i / 8] >> (i % 8)) & 0x1)
        {
            spads_enabled++;
        }
    }
    SmbusWriteReg(i2cBus, i2cAddr, GLOBAL_CONFIG_SPAD_ENABLES_REF_0, ref_spad_map[0]);
    SmbusWriteReg(i2cBus, i2cAddr, GLOBAL_CONFIG_SPAD_ENABLES_REF_0, ref_spad_map[1]);
    SmbusWriteReg(i2cBus, i2cAddr, GLOBAL_CONFIG_SPAD_ENABLES_REF_0, ref_spad_map[2]);
    SmbusWriteReg(i2cBus, i2cAddr, GLOBAL_CONFIG_SPAD_ENABLES_REF_0, ref_spad_map[3]);
    SmbusWriteReg(i2cBus, i2cAddr, GLOBAL_CONFIG_SPAD_ENABLES_REF_0, ref_spad_map[4]);
    SmbusWriteReg(i2cBus, i2cAddr, GLOBAL_CONFIG_SPAD_ENABLES_REF_0, ref_spad_map[5]);

    // -- VL53L0X_set_reference_spads() end

    // -- VL53L0X_load_tuning_settings() begin
    // DefaultTuningSettings from vl53l0x_tuning.h

    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr,0x00, 0x00);

    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x09, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x10, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x11, 0x00);

    SmbusWriteReg(i2cBus, i2cAddr,0x24, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr,0x25, 0xFF);
    SmbusWriteReg(i2cBus, i2cAddr,0x75, 0x00);

    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr,0x4E, 0x2C);
    SmbusWriteReg(i2cBus, i2cAddr,0x48, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x30, 0x20);

    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x30, 0x09);
    SmbusWriteReg(i2cBus, i2cAddr,0x54, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x31, 0x04);
    SmbusWriteReg(i2cBus, i2cAddr,0x32, 0x03);
    SmbusWriteReg(i2cBus, i2cAddr,0x40, 0x83);
    SmbusWriteReg(i2cBus, i2cAddr,0x46, 0x25);
    SmbusWriteReg(i2cBus, i2cAddr,0x60, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x27, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x50, 0x06);
    SmbusWriteReg(i2cBus, i2cAddr,0x51, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x52, 0x96);
    SmbusWriteReg(i2cBus, i2cAddr,0x56, 0x08);
    SmbusWriteReg(i2cBus, i2cAddr,0x57, 0x30);
    SmbusWriteReg(i2cBus, i2cAddr,0x61, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x62, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x64, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x65, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x66, 0xA0);

    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr,0x22, 0x32);
    SmbusWriteReg(i2cBus, i2cAddr,0x47, 0x14);
    SmbusWriteReg(i2cBus, i2cAddr,0x49, 0xFF);
    SmbusWriteReg(i2cBus, i2cAddr,0x4A, 0x00);

    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x7A, 0x0A);
    SmbusWriteReg(i2cBus, i2cAddr,0x7B, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x78, 0x21);

    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr,0x23, 0x34);
    SmbusWriteReg(i2cBus, i2cAddr,0x42, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x44, 0xFF);
    SmbusWriteReg(i2cBus, i2cAddr,0x45, 0x26);
    SmbusWriteReg(i2cBus, i2cAddr,0x46, 0x05);
    SmbusWriteReg(i2cBus, i2cAddr,0x40, 0x40);
    SmbusWriteReg(i2cBus, i2cAddr,0x0E, 0x06);
    SmbusWriteReg(i2cBus, i2cAddr,0x20, 0x1A);
    SmbusWriteReg(i2cBus, i2cAddr,0x43, 0x40);

    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x34, 0x03);
    SmbusWriteReg(i2cBus, i2cAddr,0x35, 0x44);

    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr,0x31, 0x04);
    SmbusWriteReg(i2cBus, i2cAddr,0x4B, 0x09);
    SmbusWriteReg(i2cBus, i2cAddr,0x4C, 0x05);
    SmbusWriteReg(i2cBus, i2cAddr,0x4D, 0x04);

    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x44, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x45, 0x20);
    SmbusWriteReg(i2cBus, i2cAddr,0x47, 0x08);
    SmbusWriteReg(i2cBus, i2cAddr,0x48, 0x28);
    SmbusWriteReg(i2cBus, i2cAddr,0x67, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x70, 0x04);
    SmbusWriteReg(i2cBus, i2cAddr,0x71, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr,0x72, 0xFE);
    SmbusWriteReg(i2cBus, i2cAddr,0x76, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x77, 0x00);

    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr,0x0D, 0x01);

    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x80, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr,0x01, 0xF8);

    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr,0x8E, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr,0x00, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr,0xFF, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr,0x80, 0x00);

    // -- VL53L0X_load_tuning_settings() end

    // "Set interrupt config to new sample ready"
    // -- VL53L0X_SetGpioConfig() begin

    SmbusWriteReg(i2cBus, i2cAddr,SYSTEM_INTERRUPT_CONFIG_GPIO, 0x04);
    SmbusReadReg(i2cBus, i2cAddr, GPIO_HV_MUX_ACTIVE_HIGH, &tmp);
    SmbusWriteReg(i2cBus, i2cAddr,GPIO_HV_MUX_ACTIVE_HIGH, tmp & ~0x10); // active low
    SmbusWriteReg(i2cBus, i2cAddr,SYSTEM_INTERRUPT_CLEAR, 0x01);

    // -- VL53L0X_SetGpioConfig() end

    measurement_timing_budget_us = getMeasurementTimingBudget(i2cBus, i2cAddr);

    // "Disable MSRC and TCC by default"
    // MSRC = Minimum Signal Rate Check
    // TCC = Target CentreCheck
    // -- VL53L0X_SetSequenceStepEnable() begin

    SmbusWriteReg(i2cBus, i2cAddr,SYSTEM_SEQUENCE_CONFIG, 0xE8);

    // -- VL53L0X_SetSequenceStepEnable() end

    // "Recalculate timing budget"
    setMeasurementTimingBudget(measurement_timing_budget_us, i2cBus, i2cAddr);

    // VL53L0X_StaticInit() end

    // VL53L0X_PerformRefCalibration() begin (VL53L0X_perform_ref_calibration())

    // -- VL53L0X_perform_vhv_calibration() begin
    
    SmbusWriteReg(i2cBus, i2cAddr, SYSTEM_SEQUENCE_CONFIG, 0x01);
    if (!performSingleRefCalibration(0x40, i2cBus, i2cAddr)) { return false; }

    // -- VL53L0X_perform_vhv_calibration() end

    // -- VL53L0X_perform_phase_calibration() begin

    SmbusWriteReg(i2cBus, i2cAddr,SYSTEM_SEQUENCE_CONFIG, 0x02);
    if (!performSingleRefCalibration(0x00, i2cBus, i2cAddr)) { return false; }

    // -- VL53L0X_perform_phase_calibration() end

    // "restore the previous Sequence Config"
    SmbusWriteReg(i2cBus, i2cAddr,SYSTEM_SEQUENCE_CONFIG, 0xE8);

    // VL53L0X_PerformRefCalibration() end

    return true;
}

void setAddress(uint8_t new_addr, uint8_t i2cBus,uint8_t i2cAddr) 
{
    SmbusWriteReg(i2cBus, i2cAddr, I2C_SLAVE_DEVICE_ADDRESS, (new_addr>>1) & 0x7F);
    g_i2cAddr = new_addr;
}
uint8_t getAddress() {
    return g_i2cAddr;
}

void setTimeout(uint16_t timeout){
    g_ioTimeout = timeout;
}
uint16_t getTimeout(void){
    return g_ioTimeout;
}
bool timeoutOccurred()
{
    bool tmp = g_isTimeout;
    g_isTimeout = false;
    return tmp;
}

void startContinuous(uint32_t period_ms, uint8_t i2cBus, uint8_t i2cAddr)
{
    SmbusWriteReg(i2cBus, i2cAddr, 0x80, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr, 0xFF, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr, 0x00, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr, 0x91, g_stopVariable);
    SmbusWriteReg(i2cBus, i2cAddr, 0x00, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr, 0xFF, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr, 0x80, 0x00);

    if (period_ms != 0)
    {
        // continuous timed mode

        // VL53L0X_SetInterMeasurementPeriodMilliSeconds() begin

        uint16_t osc_calibrate_val;
        SmbusReadReg16Bits(i2cBus, i2cAddr, OSC_CALIBRATE_VAL, &osc_calibrate_val);

        if (osc_calibrate_val != 0)
        {
            period_ms *= osc_calibrate_val;
        }

        uint8_t data[4];
        data[3] = period_ms & 0xFF;
        data[2] = period_ms >> 8;
        data[1] = period_ms >> 16;
        data[0] = period_ms >> 24;

        SmbusWriteReg32Bits(i2cBus, i2cAddr, SYSTEM_INTERMEASUREMENT_PERIOD, 32, data);

        // VL53L0X_SetInterMeasurementPeriodMilliSeconds() end

        SmbusWriteReg(i2cBus, i2cAddr, SYSRANGE_START, 0x04); // VL53L0X_REG_SYSRANGE_MODE_TIMED
    }
    else
    {
        // continuous back-to-back mode
        SmbusWriteReg(i2cBus, i2cAddr, SYSRANGE_START, 0x02); // VL53L0X_REG_SYSRANGE_MODE_BACKTOBACK
    }
}

uint16_t readRangeContinuousMillimeters(statInfo_t *extraStats, uint8_t i2cBus, uint8_t i2cAddr ) 
{
    // uint8_t tempBuffer[12];
    uint16_t temp;
    startTimeout();
    uint8_t tmp;
    SmbusReadReg(i2cBus, i2cAddr, RESULT_INTERRUPT_STATUS, &tmp);
    while ((tmp & 0x07) == 0) 
    {
        if (checkTimeoutExpired())
        {
            g_isTimeout = true;
            return 65535;
        }
        SmbusReadReg(i2cBus, i2cAddr, RESULT_INTERRUPT_STATUS, &tmp);
    }

    SmbusReadReg16Bits(i2cBus, i2cAddr, RESULT_RANGE_STATUS+10, &temp);
    SmbusWriteReg(i2cBus, i2cAddr, SYSTEM_INTERRUPT_CLEAR, 0x01);
    return temp;
}

uint16_t readRangeSingleMillimeters(statInfo_t *extraStats, uint8_t i2cBus, uint8_t i2cAddr) 
{
    SmbusWriteReg(i2cBus, i2cAddr, 0x80, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr, 0xFF, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr, 0x00, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr, 0x91, g_stopVariable);
    SmbusWriteReg(i2cBus, i2cAddr, 0x00, 0x01);
    SmbusWriteReg(i2cBus, i2cAddr, 0xFF, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr, 0x80, 0x00);
    SmbusWriteReg(i2cBus, i2cAddr, SYSRANGE_START, 0x01);
    // "Wait until start bit has been cleared"
    startTimeout();
    uint8_t tmp;
    SmbusReadReg(i2cBus, i2cAddr, SYSRANGE_START, &tmp);
    while (tmp & 0x01){
        if (checkTimeoutExpired()){
        g_isTimeout = true;
        return 65535;
        SmbusReadReg(i2cBus, i2cAddr, SYSRANGE_START, &tmp);
        }
    }
    return readRangeContinuousMillimeters(extraStats, i2cBus, i2cAddr);
}
