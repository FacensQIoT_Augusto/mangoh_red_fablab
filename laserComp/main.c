#include "vl53l0x.h"

COMPONENT_INIT
{

    LE_INFO("=============== I2C Reading & Writing application has started");
    if(initVL53L0X(8, 0x29))
    {
        LE_INFO("=============================================================");
        setTimeout(500);
        setMeasurementTimingBudget(200000, 8, 0x29);
        statInfo_t X;
        while(true)
        {
            uint16_t meas = readRangeSingleMillimeters(&X, 8, 0x29);
            if(timeoutOccurred())
            {
                LE_INFO("TIMEOUT");
            }
            else
            {
                LE_INFO("******* %d ********", meas);
            }
            sleep(1);
        }

    }

    else
    {
        LE_INFO("FAILED TO INIT");
    }

}