#include "ADC-sensor.h"

void initSensor(adcSensor *sensor, int _pin, double _CALIBRATION) {
	sensor->pin = _pin;
    sensor->ADC_NAME[0] = 'E';
    sensor->ADC_NAME[1] = 'X';
    sensor->ADC_NAME[2] = 'T';
    sensor->ADC_NAME[3] = '_';
    sensor->ADC_NAME[4] = 'A';
    sensor->ADC_NAME[5] = 'D';
    sensor->ADC_NAME[6] = 'C';
    sensor->ADC_NAME[8] = '\0';
    if(_pin == 0)
    {
        sensor->ADC_NAME[7] = '0';
        sensor->RESOLUTION = 15;
    }
    else if (_pin == 1)
    {
        sensor->ADC_NAME[7] = '1';
        sensor->RESOLUTION = 12;
    }
    else
    {
        sensor->ADC_NAME[7] = '2';
        sensor->RESOLUTION = 12;
    }
    sensor->OFFSET = ((1 << sensor->RESOLUTION) >> 1)/10;
    sensor->CALIBRATION = _CALIBRATION;
}

uint32_t millis()
{
    le_clk_Time_t t = le_clk_GetAbsoluteTime();
    time_t s = t.sec;
    long us = t.usec;
    return (s*1000 + us/1000);
}

double getMeas(adcSensor *sensor, unsigned int timeout, unsigned int crossings) {
    int VREF = 1800;
    int32_t tmp = 0;
    double sum = 0;
    unsigned int crossCount = 0;
    unsigned int numberOfSamples = 0;                       

    unsigned long start = millis();
    while(1)
    {
        le_adc_ReadValue(sensor->ADC_NAME, &tmp);
        startV = tmp;
        if ((tmp < ((1 << sensor->RESOLUTION)*0.55)) && (tmp > ((1 << sensor->RESOLUTION)*0.45))) break;
        if ((millis()-start) > timeout) break;
    }

    start = millis();

    while ((crossCount < crossings) && ((millis()-start) < timeout))
    {

        numberOfSamples++;
        lastFiltered = filtered;

        le_adc_ReadValue(sensor->ADC_NAME, &tmp);
        sample = tmp;

        sensor->OFFSET = sensor->OFFSET + ((sample - sensor->OFFSET)/1024);
        filtered = sample - sensor->OFFSET;

        sum += (filtered * filtered);                          
        phaseShiftedV = lastFiltered + sensor->PHASECAL * (filtered - lastFiltered);

        lastVCross = checkVCross;
        if (sample > startV) checkVCross = true;
                        else checkVCross = false;
        if (numberOfSamples==1) lastVCross = checkVCross;

        if (lastVCross != checkVCross) crossCount++;
    }

    double RATIO = sensor->CALIBRATION *((VREF/1000.0) / ((1 << sensor->RESOLUTION)));
    RATIO = RATIO * sqrt(sum / numberOfSamples);

    sum = 0;
    return RATIO;
}
