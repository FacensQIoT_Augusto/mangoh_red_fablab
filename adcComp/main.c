# include "ADC-sensor.h"

COMPONENT_INIT
{
    LE_INFO("---------------------- ADC Reading started");
    adcSensor Voltage_Sensor;
    initSensor(&Voltage_Sensor, 0, 655);
    float meas;
    while(1)
    {
        meas = getMeas(&Voltage_Sensor, 200, 200);
        LE_INFO("%f", meas);
    }
}