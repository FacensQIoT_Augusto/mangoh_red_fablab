#ifndef _ADC_SENSOR_H_
#define _ADC_SENSOR_H_

#include "legato.h"
#include "interfaces.h"

double lastFiltered,filtered;
int sample;
double phaseShiftedV;
bool lastVCross, checkVCross;
int startV;

typedef struct{
	int pin;
    char ADC_NAME[8];
    unsigned int RESOLUTION;
    double PHASECAL, CALIBRATION, OFFSET;
}adcSensor;

void initSensor(adcSensor *sensor, int _pin, double _CALIBRATION);
uint32_t millis();
double getMeas(adcSensor *sensor, unsigned int timeout, unsigned int crossings);


#endif